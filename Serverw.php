<?php

/**
 * Serverw class
 * 
 * Clase Server  Desarrollo Web en Entorno Servidor Tema 6: Servicios web 
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class Serverw extends \SoapClient {

  const WSDL_FILE = "http://daw-15/lab/tarea1/serviciow.wsdl";
  private $classmap = array(
                                   );

  public function __construct($wsdl = null, $options = array()) {
    foreach($this->classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    if(isset($options['headers'])) {
      $this->__setSoapHeaders($options['headers']);
    }
    parent::__construct($wsdl ?: self::WSDL_FILE, $options);
  }

  /**
   * Obtiene el PVP de un producto 
   *
   * @param string $codigo
   * @return float
   */
  public function getPVP($codigo) {
    return $this->__soapCall('getPVP', array($codigo),       array(
            'uri' => 'http://daw-15/lab/tarea1',
            'soapaction' => ''
           )
      );
  }

  /**
   * Obtiene el stock de un producto en una tienda 
   *
   * @param string $codigoProducto
   * @param int $codigoTienda
   * @return int
   */
  public function getStock($codigoProducto, $codigoTienda) {
    return $this->__soapCall('getStock', array($codigoProducto, $codigoTienda),       array(
            'uri' => 'http://daw-15/lab/tarea1',
            'soapaction' => ''
           )
      );
  }

  /**
   * Obtiene las familias 
   *
   * @param 
   * @return stringArray
   */
  public function getFamilias() {
    return $this->__soapCall('getFamilias', array(),       array(
            'uri' => 'http://daw-15/lab/tarea1',
            'soapaction' => ''
           )
      );
  }

  /**
   * Obtiene los productos por familia 
   *
   * @param string $codigoFamilia
   * @return stringArray
   */
  public function getProductosFamilia($codigoFamilia) {
    return $this->__soapCall('getProductosFamilia', array($codigoFamilia),       array(
            'uri' => 'http://daw-15/lab/tarea1',
            'soapaction' => ''
           )
      );
  }

}

?>
