<?php

$url = 'http://daw-15/lab/tarea1/servicio.php';
$uri = 'http://daw-15/lab/tarea1';

$cliente = new SoapClient(null, array('location' => $url, 'uri' => $uri));

echo "<strong>Todas las familias:</strong><br>";
$datos = $cliente->getFamilias();
foreach ($datos as $key) {
  echo "- " . $key . "<br>";
}
echo "<br>";

echo "<strong>Todos los productos de TV:</strong><br>";
$datos = $cliente->getProductosFamilia("TV");
foreach ($datos as $key) {
  echo "- " . $key . "<br>";
}
echo "<br>";

$datos = $cliente->getStock("3DSNG",1);
echo "<strong>El stock de una Nintendo 3DS en la tienda 1 es de:</strong> " . $datos . "<br>";

echo "<br>";
$datos = $cliente->getPVP("3DSNG");
echo "<strong>El precio de una Nintendo 3DS:</strong> $datos Euros";

?>