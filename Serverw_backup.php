<?php

require_once 'DB.php';

/**
* Clase Server
*
* Desarrollo Web en Entorno Servidor
* Tema 6: Servicios web
* @author Gabriel Longás
*/

class Serverw
{
  /**
  * Obtiene el PVP de un producto
  * @param string $codigo
  * @return float
  */
  public function getPVP($codigo)
  {
    $producto = DB::obtieneProducto($codigo);
    return $producto->getPVP();
  }

  /**
  * Obtiene el stock de un producto en una tienda
  * @param string $codigoProducto
  * @param int $codigoTienda
  * @return int
  */
  public function getStock($codigoProducto, $codigoTienda)
  {
    $stock = DB::obtieneStock($codigoProducto, $codigoTienda);
    return $stock;
  }

  /**
  * Obtiene las familias
  * @return string[]
  */
  public function getFamilias()
  {
    return DB::obtieneFamilias();
  }

  /**
  * Obtiene los productos por familia
  * @param string $codigoFamilia
  * @return string[]
  */
  public function getProductosFamilia($codigoFamilia)
  {
    return DB::obtieneProductosFamilia($codigoFamilia);
  }
}

?>