<?php

require_once 'DB.php';

class Server
{
  public function getPVP($codigo)
  {
    $producto = DB::obtieneProducto($codigo);
    return $producto->getPVP();
  }

  public function getStock($codigoProducto, $codigoTienda)
  {
    $stock = DB::obtieneStock($codigoProducto, $codigoTienda);
    return $stock;
  }

  public function getFamilias()
  {
    return DB::obtieneFamilias();
  }

  public function getProductosFamilia($codigoFamilia)
  {
    return DB::obtieneProductosFamilia($codigoFamilia);
  }
}

?>